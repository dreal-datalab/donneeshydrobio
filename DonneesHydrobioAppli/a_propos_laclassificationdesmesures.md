# La classification des mesures et les seuils associés

[![](www/Guide_tech.png)](https://www.eaufrance.fr/sites/default/files/2019-05/guide-reee-esc-2019-cycle3.pdf)

Afin d'avoir des résultats homogènes entre les différents indices biologiques, les notes de l'Indice Biologique Diatomées (IBD) et de l'Indice Biologique Macrophytes en Rivière (IBMR) ont été recalculés pour être transformés en écart à la référence (EQR). Actuellement, aucune méthode n'a été trouvé pour transformé les notes IPR en EQR. De ce fait, les classes de qualité sont appliqué directement sur les notes IPR. L'ensemble des indices ont ensuite été traduits en classe de qualité.

Exemple de la station Boulay à Mareil-sur-Loir (04615011) qui a une note IBMR de 4,5 qui donne un EQR de 0,40286 et qui correspond à une classe de qualité « Mauvaise ».

Les listes taxonomiques ont été transformées en données de présence (1) / absence (0).

Pour les suivis des différents indices biologiques, les seuils utilisés sont ceux définit dans le *[Guide technique](https://www.eaufrance.fr/sites/default/files/2019-05/guide-reee-esc-2019-cycle3.pdf) relatif à l'évaluation de l'état des eaux de surface continentales (cours d'eau, canaux, plans d'eau)* de janvier 2019. Les classes de qualité pour les macroinvertébrés (I2M2), les macrophytes (IBMR) et les diatomées sont disponible dans le tableau ci-dessous.

![](www/Image_Tab.png)

