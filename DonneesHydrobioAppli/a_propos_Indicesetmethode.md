# Les indices hydrobiologique et la méthode utilisés

## Les indices hydrobiologiques

### Les macroinvertébrés

[![](www/Macroinvertebre.png)](http://www.pays-de-la-loire.developpement-durable.gouv.fr/le-suivi-des-macro-invertebres-a5593.html)

Les macroinvertébrés sont des bio-indicateurs composés principalement d'insectes, de crustacés, de mollusques et de vers. Il sont utilisés pour évaluer la qualité des habitats aquatiques. Au cours du temps différents indices ont été developpé pour être plus stable et représentatif des milieux étudiés.

Actuellement, et depuis 2015, l'Indice Invertébrés Multi_Métriques (I2M2) est utilisé car c'est l'indice le plus adapté et compatible avec la DCE. Avant 2015, l'Indice Biologique Global (IBG) avait été developpé pour faire le lien entre l'Indice Biologique Global Normalisé (IBGN) et l'I2M2 afin de pouvoir comparer l'ensemble des résultats.

L' **IBG** permet de donner une note équivalente à l'IBGN mais aussi une note EQR (Ecological Quality Ratio) de 0 à 1 de la station.

L' **I2M2** a été développé pour être plus stable et plus représentatif de la diversité des habitats que l'IBG compatible avec la DCE. Il est basé sur le calcul en EQR de **cinq métriques** élémentaires intégrant la notion de polluo-sensibilité :

-   La richesse taxonomique

-   L'indice de Shannon

-   L'Average Score Per Taxon (ASPT)

-   Le pourcentage de taxons polyvoltins

-   Le pourcentage de taxons ovovivipares



[![](www/Macrophyte.png)](http://www.pays-de-la-loire.developpement-durable.gouv.fr/le-suivi-des-macrophytes-a5594.html)
### Les macrophytes

Les macrophytes sont les végétaux visibles à l'oeil nu (Algues, bryophytes, hydrophytes et hélophytes) et permettent d'évaluer l'eutrophie (la quantité de nutriment) des cours d'eau.

L'Indice Biologique Macrophyte en Rivière ( **IBMR**) est utilisé pour déterminer cette trophie. Cet indice donne une note de trophie et la comparaison avec le niveau de référence donne une note en EQR de l'état biologique du cours d'eau pour cet indice.

### Les diatomées
[![](www/diatomee.png)](http://www.pays-de-la-loire.developpement-durable.gouv.fr/le-suivi-des-diatomees-a5592.html)

Les diatomées sont des algues unicellulaires sensibles à la pollution, la dégradation du milieux mais aussi aux micropolluants.

L'Indice Biologique Diatomées ( **IBD**) donne une note de qualité du cours d'eau en fonction de l'abondance et la sensibilité des individus retrouvés a différents paramètres des cours d'eau. Cet indice est lié à l'Indice de Polluo-sensibilité spécifique ( **IPS**) qui est plus sensible aux valeurs extrêmes.

### Les poissons
Les poissons sont de bon indicateurs biologique. Situé en fin de chaine alimentaire dans les cours d'eau, il permet de mettre en avant les perturbations que subit le milieu.

L'Indice Poisson Rivière ( **IPR**) permet mettre en avant si les poissons présents dans les cours d'eau sont conformes au peuplement théorique de référence.
(Source : [Eau Maine et Loire](https://eau.maine-et-loire.fr/surveiller-et-proteger/qualite-des-rivieres/indicateurs/indice-poissons))

*crédits photo : Laboratoire d'hydrobiologie de la DREAL des Pays de la Loire*


## La méthode utilisée

### La régression linéaire

La regression linéaire permet de mettre en avant une évolution globale de l'ensemble du jeu de données pour en faire ressortir une tendance (Augmentation, baisse, stable).

### La régression LOESS

La LOcal regRESSion (LOESS) est une méthode qui permet de produire des courbes lissées et ajustées à des nuages de points. Cette méthode permet de mettre en avant des variations de tendance des données.
