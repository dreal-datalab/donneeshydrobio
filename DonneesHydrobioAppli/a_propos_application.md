# L'application

__Cette application met à disposition les données sur les indicateurs hydrobiologiques dans les cours d’eau de la région Pays de la Loire à partir de 2007__.


La préservation des milieux aquatiques est importante pour la conservation de la vie aquatique mais aussi pour répondre aux différents usages anthropiques de ces milieux. Dans ce contexte la Directive Cadre sur l’Eau (DCE) a été adoptée en Europe et doit permettre de prévenir toute dégradation supplémentaire, préserver et améliorer l’état des écosystèmes aquatiques. En France, cette directive est planifiée par les Schémas Directeurs d’Aménagement et de Gestion des Eaux (SDAGE) en s’appuyant sur un programme de surveillance. Ce programme permet de mesurer et qualifier chaque masse d'eau souterraine et superficielle.


Cette application met à disposition, à partir des données bancarisées par l'[Agence de l'Eau Loire-Bretagne](https://agence.eau-loire-bretagne.fr/) sur le site [Naiades](http://www.naiades.eaufrance.fr/), une partie des données qui permettent de mesurer la qualité des cours d'eau : il s'agit des données relatives à l'hydrobiologie : mesure des macroinvertébrés, des diatomées, des marcophytes et des poissons. Ces données ont été recueillies depuis 2007 dans le cadre de ce programme de surveillance sur l’ensemble des Pays de la Loire.


Les concentrations en nitrates sont disponibles par station ou regroupées à la masse d’eau pour les eaux superficielles, et par station pour les eaux souterraines.


L’ensemble des données (indices, listes taxonomiques...) peut être téléchargé dans la rubrique « Télécharger les données ».

En outre, les valorisations graphiques  vous sont proposées et sont issus d'un travail interne à la [DREAL Pays de la Loire](http://www.pays-de-la-loire.developpement-durable.gouv.fr/le-laboratoire-d-hydrobiologie-de-la-dreal-r1512.html) : ces résultats ne se substituent pas aux données et résultats de l'état des lieux du bassin Loire-Bretagne  et ne sont qu'une proposition d'analyse de l'évolution constatée.
    
    
Ces données seront mises à jour régulièrement et l'application enrichie au fur et à mesure.


N'hésitez pas à [nous contacter](mailto:srnp.dreal-paysdelaloire@developpement-durable.gouv.fr?subject=Application shiny hydrobiologie&body=Bonjour,") pour toute remarque ou suggestion.


