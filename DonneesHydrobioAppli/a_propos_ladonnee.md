# La donnée

![](www/Image_apropos_ladonnees.png)

Les données sont prélevées par les prestataires de l'Agence de l'Eau ayant des lots du marché de surveillance, ainsi que par le laboratoire de la [DREAL des Pays de la Loire](http://www.pays-de-la-loire.developpement-durable.gouv.fr/le-laboratoire-d-hydrobiologie-de-la-dreal-r1512.html).

La base de données a été créée en quatre parties à partir de nombreuses données non exhaustives sur le territoire.

-   Les **informations sur les stations** : Les bases Naïades, Sandre et internes à la DREAL des Pays de la Loire ;

-   Les données **physico-chimiques** : La base Naïades [En ligne];

-   Les données **biologiques** : L'Agence de l'Eau Loire Bretagne (AELB) ;

-   Les **listes taxonomiques** (uniquement macroinvertébrés, macrophytes et diatomées): L'Agence de l'Eau Loire Bretagne (AELB).

Les données Sandre et Naiades sont disponibles en open data sur les sites correspondants ou via Hub'eau :

-   <https://sandre.eaufrance.fr/>
-   <http://www.naiades.eaufrance.fr/>

Ces données seront mises à jour régulièrement et l'application enrichie au fur et à mesure.

Les données alimentant la v1 correspondent à un export sur la période 2007-2020 de l'Agence de l'Eau (à noter que l'année 2020 est incomplète) et un export Naiades réalisés en mars 2021.

*crédits photo : Laboratoire d'hydrobiologie de la DREAL des Pays de la Loire*
